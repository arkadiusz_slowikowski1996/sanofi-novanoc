﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ProgressGraph : LayoutScreen
{
    public enum STATIONS_MODE { HYPNOGRAM, QUESTION }

    private static ProgressGraph instance;
    private static ProgressGraph Instance
    {
        get
        {
            if (!instance)
            {
                ProgressGraph[] allInstances = Resources.FindObjectsOfTypeAll(typeof(ProgressGraph))as ProgressGraph[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    [SerializeField] private GameObject progressGraphGameObject;
    [SerializeField] private Image sheep;
    [SerializeField] private RectTransform zip;
    [SerializeField] private float sheepLerpingSpeed = 3;
    [SerializeField] private Locker[] lockers;
    [SerializeField] private Transform[] stations;
    [SerializeField] private Transform[] stationsHypnogramTransforms;
    [SerializeField] private Transform[] stationsQuestionTransforms;
    [SerializeField] private float stationsLerpingSpeed = 3;
    [SerializeField] private Sprite[] sheepStageSprites;
    [SerializeField] private Sprite sheepJumpingSprite;

    private static int currentStage;

    private Coroutine sheepCoroutine;
    private Coroutine stationsCoroutine;

    private float ZipMin => stations[0].localPosition.x; //zip.position.x - zip.rect.width / 2;
    private float ZipMax => stations[stations.Length - 1].localPosition.x; //zip.position.x + zip.rect.width / 2;

    //public static float Progress
    //{
    //    set
    //    {
    //        value = Mathf.Clamp(value, 0, 1);

    //        if (!Instance.gameObject.activeInHierarchy) Instance.gameObject.SetActive(true);
    //        if (Instance.sheepCoroutine != null) GameMode.StopCoroutineOnMe(Instance.sheepCoroutine);
    //        Instance.sheepCoroutine = GameMode.StartCoroutineOnMe(Instance.MoveSheep(Mathf.Lerp(Instance.ZipMin, Instance.ZipMax, value)));
    //        Instance.lockers[(int)(value * 2f)].TurnOffLockedImage();
    //        if ((int)(value * 2f) - 1 >= 0) Instance.lockers[(int)(value * 2f) - 1].TurnOnUnlockedImage();
    //    }
    //}

    private Transform[, ] AllStationsTransforms
    {
        get
        {
            Transform[, ] returnValue = new Transform[2, 3];

            for (int i = 0; i < returnValue.GetLength(0); i++)
                for (int j = 0; j < returnValue.GetLength(1); j++)
                    returnValue[i, j] = i == 0 ? stationsHypnogramTransforms[j] : stationsQuestionTransforms[j];

            return returnValue;
        }
    }

    private STATIONS_MODE stationsMode;
    public static STATIONS_MODE StationsMode
    {
        get => Instance.stationsMode;

        set
        {
            if (Instance.stationsMode != value)
            {
                Instance.stationsMode = value;
                if (Instance.stationsCoroutine != null)GameMode.StopCoroutineOnMe(Instance.stationsCoroutine);
                Instance.stationsCoroutine = GameMode.StartCoroutineOnMe(Instance.MoveStations());
            }
        }
    }

    private void Awake()
    {
        instance = this;

        Hypnogram.StageChanged += Hypnogram_StageChanged;
    }

    private void Hypnogram_StageChanged(int stage, int outOf)
    {
        Instance.progressGraphGameObject.SetActive(true);

        MoveToStage(stage);
    }

    //private IEnumerator MoveSheep(float destPositionX)
    //{
    //    while (sheep.transform.localPosition.x != destPositionX)
    //    {
    //        Vector3 destPosition = new Vector3(
    //            destPositionX,
    //            sheep.transform.localPosition.y,
    //            sheep.transform.localPosition.z
    //            );

    //        sheep.transform.localPosition = Vector3.Lerp(sheep.transform.localPosition, destPosition, sheepLerpingSpeed * Time.deltaTime);

    //        yield return 0;
    //    }

    //    sheepCoroutine = null;
    //}
    public static IEnumerator DeactivateWrap(bool sudden = false)
    {
        yield return GameMode.StartCoroutineOnMe(Instance.Deactivate(sudden));
    }

    private IEnumerator MoveStations()
    {
        while (stations[0].position != AllStationsTransforms[(int)stationsMode, 0].position)
        {
            for (int i = 0; i < stations.Length; i++)
            {
                Transform dest = AllStationsTransforms[(int)stationsMode, i];

                stations[i].position = Vector3.Lerp(stations[i].position, dest.position, stationsLerpingSpeed * Time.deltaTime);
                stations[i].localScale = Vector3.Lerp(stations[i].localScale, dest.localScale, stationsLerpingSpeed * Time.deltaTime);
            }

            yield return 0;
        }

        stationsCoroutine = null;
    }

    private static void MoveToStage(int stage)
    {
        currentStage = stage;

        if (Instance.sheepCoroutine != null)GameMode.StopCoroutineOnMe(Instance.sheepCoroutine);

        Vector3 newPos = Instance.sheep.transform.localPosition;

        if (stage < Instance.lockers.Length)
        {
            newPos = Mathf.LerpUnclamped(Instance.ZipMin, Instance.ZipMax, (float)stage / 2f) * Vector3.right;
        }
        else
        {
            GameMode.StartCoroutineOnMe(Fade(0, 3, false, Instance.sheep));
        }

        Instance.sheepCoroutine = GameMode.StartCoroutineOnMe(Instance.MovementJump(newPos.x));

        if (stage < Instance.lockers.Length)Instance.lockers[stage].TurnOffLockedImage();
        if (stage - 1 >= 0)Instance.lockers[stage - 1].TurnOnUnlockedImage();

        GameMode.StartCoroutineOnMe(Instance.WaitForJumpingToFinishAndChangeSheepsSprite());
    }

    private IEnumerator WaitForJumpingToFinishAndChangeSheepsSprite()
    {
        sheep.sprite = sheepJumpingSprite;

        yield return Instance.sheepCoroutine;

        sheep.sprite = sheepStageSprites[Mathf.Clamp(currentStage, 0, sheepStageSprites.Length - 1)];
    }

    [SerializeField] private int numberOfJumps;
    [SerializeField] private float startA;
    [SerializeField] private int jumpsSpeed;

    private IEnumerator MovementJump(float newX)
    {
        float F(float _x, float _a, float _x0, float _x1)
        {
            float val = _a * (_x - _x0) * (_x - _x1);
            return val;
        }

        Vector3 startPos = sheep.transform.localPosition;
        float currJumpsSpeed = jumpsSpeed;
        float totalDistance = newX - startPos.x;
        float distanceMade = 0;
        float distanceBetweenJumps = totalDistance / numberOfJumps;

        float x0 = 0, x1 = 0;
        float a = startA;

        while (distanceMade < totalDistance)
        {
            x0 = sheep.transform.localPosition.x;
            x1 = sheep.transform.localPosition.x + distanceBetweenJumps;

            while (sheep.transform.localPosition.x < x1)
            {
                distanceMade += Time.deltaTime * jumpsSpeed;

                Vector3 newPos = Vector3.zero;
                newPos.x = distanceMade + startPos.x;
                newPos.y = F(newPos.x, a, x0, x1) + startPos.y;

                sheep.transform.localPosition = newPos;

                yield return 0;
            }

            a *= 0.7f;
            currJumpsSpeed *= 1.4f;

            yield return 0;
        }
    }

    private IEnumerator ActivateWrapper()
    {
        yield return GameMode.StartCoroutineOnMe(base.Activate());
    }

    public new static IEnumerator Activate()
    {
        yield return GameMode.StartCoroutineOnMe(Instance.ActivateWrapper());
    }

    private IEnumerator DeactivateWrapper()
    {
        yield return GameMode.StartCoroutineOnMe(base.Deactivate());
    }

    public new static IEnumerator Deactivate()
    {
        yield return GameMode.StartCoroutineOnMe(Instance.DeactivateWrapper());
    }
}