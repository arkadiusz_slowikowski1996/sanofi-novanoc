﻿using UnityEngine;
using UnityEngine.UI;

public class NNProgressBar : ProgressBar
{
    [SerializeField] private Transform stary;
    [SerializeField] private Image circle;
    [SerializeField] private Image crosshair;

    private Coroutine coroutine;

    public override float Progress
    {
        set
        {
            bool newActive = value != 0;

            if (newActive != gameObject.activeInHierarchy)
            {
                if (coroutine != null) GameMode.StopCoroutineOnMe(coroutine);

                coroutine = GameMode.StartCoroutineOnMe(LayoutScreen.Fade(newActive ? 1 : 0, 1, false, GetComponentsInChildren<Graphic>()));
            }

            crosshair.color = new Color(crosshair.color.r, crosshair.color.g, crosshair.color.b, (1 - circle.color.a) * 0.5f);

            gameObject.SetActive(value != 0);

            value = 1 - Mathf.Clamp(value, 0, 1);

            float max = 1;
            value = Mathf.Pow(value, 2.5f);

            float step = 1 - value / max;

            stary.localEulerAngles = Mathf.Lerp(50, 330, step) * Vector3.forward;
            transform.localEulerAngles = Mathf.Lerp(250, 315, step) * Vector3.forward;
            circle.fillAmount = Mathf.Lerp(0.08f, 0.92f, step);
        }
    }
}