﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LayoutScreen : MonoBehaviour
{
    [SerializeField] protected float fadeTime = 3;

    public virtual IEnumerator Activate()
    {
        //GameMode.CanButtonsBeUsed = false;
        gameObject.SetActive(true);
        yield return GameMode.StartCoroutineOnMe(Fade(1, fadeTime, false, GetComponentsInChildren<Graphic>()));
        //GameMode.CanButtonsBeUsed = true;
    }

    public virtual IEnumerator Deactivate(bool sudden = false)
    {
        //GameMode.CanButtonsBeUsed = false;
        if (!sudden)
        {
            yield return GameMode.StartCoroutineOnMe(Fade(0, fadeTime, false, GetComponentsInChildren<Graphic>()));
        }
        gameObject.SetActive(false);
        //GameMode.CanButtonsBeUsed = true;
    }

    public static IEnumerator Fade(int destAlpha, float fadeTime, bool turnOffWhenDone, params Graphic[] targets)
    {
        float currTime = 0;
        fadeTime /= Mathf.Pow((1 + (1 - destAlpha)), 1.25f);
        while (currTime <= fadeTime)
        {
            foreach (Graphic g in targets)
            {
                try
                {
                    Color color = g.color;
                    color.a = Mathf.Lerp(1 - destAlpha, destAlpha, currTime / fadeTime);
                    g.color = color;
                }
                catch
                {
                    continue;
                }
            }

            currTime += Time.deltaTime;

            yield return 0;
        }

        if (turnOffWhenDone)
            foreach (Graphic g in targets)
                g.gameObject.SetActive(false);
    }
}