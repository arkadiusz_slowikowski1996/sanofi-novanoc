﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SheepCompanion : MonoBehaviour
{
    [SerializeField] private Image companionSheep;
    [SerializeField] private Image sheepToFollow;
    [SerializeField] private Image questionMark;
    [SerializeField] private Image exclamationMark;

    [SerializeField] private Sprite newQuestionSprite;
    [SerializeField] private Sprite correntAnswerSprite;
    [SerializeField] private Sprite wrongAnswerSprite;
    [SerializeField] private float eventSpriteDuration;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip bigBell;
    [SerializeField] private AudioClip smallBell;

    private bool followSmallSheepsSprite = false;
    private Sprite previousFollowedSprite;

    private void Awake()
    {
        Hypnogram.StageChanged += Hypnogram_StageChanged;
        Question.NewQuestionAppeared += Question_NewQuestionAppeared;
        AnswerButton.AnswerGiven += AnswerButton_AnswerGiven;
    }

    private IEnumerator Start()
    {
        yield return 0;
        previousFollowedSprite = sheepToFollow.sprite;
        yield return 0;
        followSmallSheepsSprite = true;
    }

    private void Update()
    {
        if (followSmallSheepsSprite)
        {
            if (sheepToFollow.sprite != previousFollowedSprite)
            {
                audioSource.clip = bigBell;
                audioSource.Play();
            }

            companionSheep.sprite = sheepToFollow.sprite;

            previousFollowedSprite = companionSheep.sprite;
        }
    }

    private void Hypnogram_StageChanged(int stage, int outOf)
    {
        companionSheep.enabled = true;
        companionSheep.color = new Color(1, 1, 1, 0);

        GameMode.StartCoroutineOnMe(LayoutScreen.Fade(1, 3, false, companionSheep));

        Hypnogram.StageChanged -= Hypnogram_StageChanged;
    }

    private void Question_NewQuestionAppeared()
    {
        //GameMode.StartCoroutineOnMe(TemporarilyChangeSprite(newQuestionSprite));
        GameMode.StartCoroutineOnMe(TemporarilyEnableAddditionalImage(questionMark));
    }

    private void AnswerButton_AnswerGiven(bool isCorrect)
    {
        //GameMode.StartCoroutineOnMe(TemporarilyChangeSprite(isCorrect ? correntAnswerSprite : wrongAnswerSprite));
        GameMode.StartCoroutineOnMe(TemporarilyEnableAddditionalImage(isCorrect ? exclamationMark : exclamationMark));
    }

    private IEnumerator TemporarilyChangeSprite(Sprite newSprite)
    {
        followSmallSheepsSprite = false;
        companionSheep.sprite = newSprite;

        yield return new WaitForSeconds(eventSpriteDuration / Time.timeScale);

        followSmallSheepsSprite = true;
    }

    private IEnumerator TemporarilyEnableAddditionalImage(Image temporaryImage)
    {
        audioSource.clip = smallBell;
        audioSource.Play();

        temporaryImage.gameObject.SetActive(true);

        yield return new WaitForSeconds((temporaryImage == questionMark ? 2 : 1 * eventSpriteDuration) / Time.timeScale);

        temporaryImage.gameObject.SetActive(false);
    }
}