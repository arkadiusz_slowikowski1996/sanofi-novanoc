﻿using System.Collections;
using UnityEngine;

public class LectorMode : MonoBehaviour
{
    private static LectorMode instance;
    private static LectorMode Instance
    {
        get
        {
            if (!instance)
            {
                LectorMode[] allInstances = Resources.FindObjectsOfTypeAll(typeof(LectorMode)) as LectorMode[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private float soundVolume;

    private void Update()
    {
        audioSource.pitch = Time.timeScale;
    }

    private void PlayClip(AudioClip clip)
    {
        audioSource.PlayOneShot(clip, soundVolume);
    }

    private static IEnumerator PlayWithWaiting(AudioClip clip)
    {
        if (Instance.audioSource.isPlaying)
        {
            Instance.audioSource.Stop();
        }

        Instance.PlayClip(clip);

        while (Instance.audioSource.isPlaying)
        {
            yield return 0;
        }
    }

    public static void Stop()
    {
        Instance.audioSource.Stop();
    }

    public static bool isPlaying => Instance.audioSource.isPlaying;

    [SerializeField] private AudioClip start;
    public static IEnumerator PlayWithWaiting_Start() => PlayWithWaiting(Instance.start);

    [SerializeField] private AudioClip wykres1;
    public static IEnumerator PlayWithWaiting_Wykres1() => PlayWithWaiting(Instance.wykres1);

    [Header("QUESTION 1")]

    [SerializeField] private AudioClip pytanie1;
    public static IEnumerator PlayWithWaiting_Pytanie1() => PlayWithWaiting(Instance.pytanie1);

    [SerializeField] private AudioClip odp1_1;
    public static IEnumerator PlayWithWaiting_Odp1_1() => PlayWithWaiting(Instance.odp1_1);

    [SerializeField] private AudioClip odp1_2;
    public static IEnumerator PlayWithWaiting_Odp1_2() => PlayWithWaiting(Instance.odp1_2);

    [SerializeField] private AudioClip odp1_3;
    public static IEnumerator PlayWithWaiting_Odp1_3() => PlayWithWaiting(Instance.odp1_3);

    [SerializeField] private AudioClip skladnik1;
    public static IEnumerator PlayWithWaiting_Skladnik1() => PlayWithWaiting(Instance.skladnik1);

    [Header("QUESTION 2")]

    [SerializeField] private AudioClip pytanie2;
    public static IEnumerator PlayWithWaiting_Pytanie2() => PlayWithWaiting(Instance.pytanie2);

    [SerializeField] private AudioClip odp2_1;
    public static IEnumerator PlayWithWaiting_Odp2_1() => PlayWithWaiting(Instance.odp2_1);

    [SerializeField] private AudioClip odp2_2;
    public static IEnumerator PlayWithWaiting_Odp2_2() => PlayWithWaiting(Instance.odp2_2);

    [SerializeField] private AudioClip odp2_3;
    public static IEnumerator PlayWithWaiting_Odp2_3() => PlayWithWaiting(Instance.odp2_3);

    [SerializeField] private AudioClip skladnik2;
    public static IEnumerator PlayWithWaiting_Skladnik2() => PlayWithWaiting(Instance.skladnik2);

    [Header("QUESTION 3")]

    [SerializeField] private AudioClip pytanie3;
    public static IEnumerator PlayWithWaiting_Pytanie3() => PlayWithWaiting(Instance.pytanie3);

    [SerializeField] private AudioClip odp3_1;
    public static IEnumerator PlayWithWaiting_Odp3_3() => PlayWithWaiting(Instance.odp3_1);

    [SerializeField] private AudioClip odp3_2;
    public static IEnumerator PlayWithWaiting_Odp3_2() => PlayWithWaiting(Instance.odp3_2);

    [SerializeField] private AudioClip odp3_3;
    public static IEnumerator PlayWithWaiting_Odp3_1() => PlayWithWaiting(Instance.odp3_3);

    [SerializeField] private AudioClip skladnik3;
    public static IEnumerator PlayWithWaiting_Skladnik3() => PlayWithWaiting(Instance.skladnik3);

    [Header("ENDING")]

    [SerializeField] private AudioClip kompletSkladnikow;
    public static IEnumerator PlayWithWaiting_KompletSkladnikow() => PlayWithWaiting(Instance.kompletSkladnikow);
    [SerializeField] private AudioClip mozdzierz;
    public static IEnumerator PlayWithWaiting_Mozdzierz() => PlayWithWaiting(Instance.mozdzierz);

    [SerializeField] private AudioClip novanoc;
    public static IEnumerator PlayWithWaiting_Novanoc() => PlayWithWaiting(Instance.novanoc);

    [SerializeField] private AudioClip koniec;
    public static IEnumerator PlayWithWaiting_Koniec() => PlayWithWaiting(Instance.koniec);

    [SerializeField] private AudioClip dziekujemy;
    public static IEnumerator PlayWithWaiting_Dziekujemy() => PlayWithWaiting(Instance.dziekujemy);
}