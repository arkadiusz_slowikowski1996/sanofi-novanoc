﻿using System;
using System.Collections;
using UnityEngine;

public class GameMode : MonoBehaviour
{
    public delegate void ShowingLogo();
    public static event ShowingLogo LogoShown;

    private static GameMode instance;

    private static bool canButtonsBeUsed;
    private static LayoutScreen currentScreen;
    [SerializeField] private LayoutScreen startScreen;

    [Range(0.01f, 10)]
    [SerializeField] private float timeSpeed = 1;

    public static bool CanButtonsBeUsed
    {
        get => canButtonsBeUsed;
        set => canButtonsBeUsed = value;
    }

    private void Awake()
    {
        if (instance)
        {
            Destroy(instance.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2 / Time.timeScale);

        LogoShown?.Invoke();

        ChangeScreen(startScreen);
        //StartCoroutineOnMe(AdScreen.Run());

        yield return new WaitForSeconds(5 / Time.timeScale);

        LocalizableText.GlobalLanguage = LocalizableText.TEXT_LANGUAGE.POLISH;
    }

    private void Update()
    {
        Time.timeScale = timeSpeed;
        if (Input.GetKeyDown(KeyCode.P))
            LocalizableText.GlobalLanguage = LocalizableText.TEXT_LANGUAGE.POLISH;

        if (Input.GetKeyDown(KeyCode.E))
            LocalizableText.GlobalLanguage = LocalizableText.TEXT_LANGUAGE.ENGLISH;
    }

    public static Coroutine StartCoroutineOnMe(IEnumerator coroutine)
    {
        return instance.StartCoroutine(coroutine);
    }

    public static void StopCoroutineOnMe(Coroutine coroutine)
    {
        instance.StopCoroutine(coroutine);
    }

    public static void ChangeScreen(LayoutScreen newScreen)
    {
        GameMode.StartCoroutineOnMe(ChangeScreenCoroutine(newScreen));
        //GameMode.StartCoroutineOnMe(ChangeScreenCoroutine(newScreen));
    }

    private static IEnumerator ChangeScreenCoroutine(LayoutScreen newScreen)
    {
        if (currentScreen)yield return GameMode.StartCoroutineOnMe(currentScreen.Deactivate());
        yield return GameMode.StartCoroutineOnMe(newScreen.Activate());
        currentScreen = newScreen;
    }
}