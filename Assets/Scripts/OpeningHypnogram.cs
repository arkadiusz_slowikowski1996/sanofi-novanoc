﻿using System.Collections;
using UnityEngine;

public class OpeningHypnogram : OpeningScreenButton
{
    protected override void OpenScreen()
    {
        GameMode.StartCoroutineOnMe(CloseCurrentAndThenRun());
    }

    private IEnumerator CloseCurrentAndThenRun()
    {
        yield return GameMode.StartCoroutineOnMe(GetComponentInParent<LayoutScreen>().Deactivate());

        yield return GameMode.StartCoroutineOnMe(Hypnogram.OpenCoroutine(Hypnogram.STAGE.FIRST));
        base.OpenScreen();
    }
}