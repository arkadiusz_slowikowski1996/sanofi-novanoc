﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class GIF : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private bool doOnce = false;
    [SerializeField] private float timeToChange = 0.5f;
    [SerializeField] private VRButton buttonAboveMe;

    private Image image;

    private int currentSprite = 0;

    public float TimeDuration => timeToChange * sprites.Length;

    private void Start()
    {
        buttonAboveMe = GetComponentInParent<VRButton>();
        image = GetComponent<Image>();
        GameMode.StartCoroutineOnMe(PlayGif());
    }

    private IEnumerator PlayGif()
    {
        while (doOnce ? currentSprite != sprites.Length : true)
        {
            if (!buttonAboveMe || buttonAboveMe.IsHovered)
            {
                yield return new WaitForSeconds(timeToChange/Time.timeScale);
                ++currentSprite;
                currentSprite %= sprites.Length;
                image.sprite = sprites[currentSprite];
            }
            else
            {
                currentSprite = 0;
                image.sprite = sprites[0];
                yield return 0;
            }
        }
    }
}
