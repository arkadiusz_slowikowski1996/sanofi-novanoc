﻿using System.Collections;
using UnityEngine;

public class ActionCanvasMovement : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private float radius = 50;
    [SerializeField] private float movingOnCircleTime = 2;
    [SerializeField] private float distance;

    private void Start()
    {
        StartCoroutine(CheckIfPlayerIsLookingAway());
    }

    private IEnumerator CheckIfPlayerIsLookingAway()
    {
        while (true)
        {
            Vector3 forward = player.forward;
            forward.y = 0;
            forward = forward.normalized;
            Vector3 forwardOnCircle = forward * radius;

            distance = Vector3.Distance(forwardOnCircle, transform.position);

            if (distance > radius * 1.5f)
            {
                transform.LookAt(player);
                Vector3 eul = transform.eulerAngles;
                eul.y += 180;
                yield return StartCoroutine(Move(forwardOnCircle, eul));
            }

            yield return 0;
        }
    }

    private IEnumerator Move(Vector3 toPos, Vector3 toRot)
    {
        float timer = 0;

        Vector3 startPos = transform.position;
        Vector3 startRot = transform.eulerAngles;

        while (timer < movingOnCircleTime)
        {
            transform.position = Vector3.Lerp(startPos, toPos, timer / movingOnCircleTime);
            transform.eulerAngles = Vector3.Lerp(startRot, toRot, timer / movingOnCircleTime);

            timer += Time.deltaTime;

            yield return 0;
        }
    }
}