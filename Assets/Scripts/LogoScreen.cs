﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoScreen : LayoutScreen
{
    [SerializeField] private float seconds;
    [SerializeField] private Image image;
    [SerializeField] private LayoutScreen screenToOpenAfterSeconds;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(seconds/Time.timeScale);
        GameMode.ChangeScreen(screenToOpenAfterSeconds);
    }
}