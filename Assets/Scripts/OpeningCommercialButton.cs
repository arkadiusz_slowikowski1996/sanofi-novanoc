﻿using UnityEngine;

public class OpeningCommercialButton : VRButton
{
    [SerializeField] private GameObject[] toClose;

    public override void Clicked()
    {
        base.Clicked(); 

        foreach(GameObject go in toClose)
        {
            go.SetActive(false);
        }

        LectorMode.Stop();

        GameMode.StartCoroutineOnMe(CommercialHandler.Run());
    }
}