﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MortarBubble : MonoBehaviour
{
    public Image image;

    private float speed;
    private float lyfeTyme;
    private float dyingTime;
    private float currentLyfeTyme;

    private Color deadColor = new Color(1, 1, 1, 0);

    private void Start()
    {
        StartCoroutine(Run());
    }

    public void Setup(float speed, float lyfeTyme, float dyingTime, Vector2 startPosition)
    {
        this.speed = speed;
        this.lyfeTyme = lyfeTyme;
        this.dyingTime = dyingTime;

        transform.localPosition = startPosition;
    }

    private IEnumerator Run()
    {
        float dyingTimer = 0;

        while (true)
        {
            transform.localPosition += Vector3.up * speed * Time.deltaTime;

            currentLyfeTyme += Time.deltaTime;

            if (currentLyfeTyme >= lyfeTyme)
            {
                if(dyingTimer <  dyingTime)
                {
                    image.color = Color.Lerp(Color.white, deadColor, dyingTimer / dyingTime);
                    dyingTimer += Time.deltaTime;
                }
                else
                {
                    Destroy(gameObject);
                }
            }

            yield return 0;
        }
    }
}