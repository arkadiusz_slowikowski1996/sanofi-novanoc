﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public abstract class VRButton : MonoBehaviour
{
    private bool recentlyClicked;
    public bool RecentlyClicked => recentlyClicked;

    public static bool generalForbid = false;

    private float progress;
    public float Progress
    {
        get => progress;
        set
        {
            //foreach (Image i in progressGraphics)
            //{
            //    i.fillAmount = value;
            //}
        }
    }

    private Graphic[] allMyGraphics;
    public Graphic[] AllMyGraphics
    {
        get
        {
            if (allMyGraphics == null)
            {
                allMyGraphics = GetComponentsInChildren<Graphic>();
            }

            return allMyGraphics;
        }
    }

    private bool isHovered = false;
    public bool IsHovered => isHovered;

    public bool canBeClicked = true;
    public bool isBetterThanOther = false;

    private BoxCollider boxCollider;
    private RectTransform rectTransform;

    //[SerializeField] public Image progressGraphic;
    [SerializeField] protected Image[] progressGraphics;
    [SerializeField] private GIF[] gifsUnderMe;

    public virtual void Clicked() => recentlyClicked = true;
    public virtual void Hovered() => isHovered = true;
    public virtual void Unhovered()
    {
        recentlyClicked = false;
        isHovered = false;
    }

    public void OnValidate()
    {
        if (!boxCollider) boxCollider = GetComponent<BoxCollider>();
        if (!rectTransform) rectTransform = GetComponent<RectTransform>();

        boxCollider.size = new Vector3(rectTransform.rect.width, rectTransform.rect.height, 1);
    }
}

//[CustomEditor(typeof(AnswerButton))]
//public class VRButton_CustomEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        base.OnInspectorGUI();

//        if (GUILayout.Button("SAMOCHOD"))
//        {
//            foreach (VRButton vrb in FindObjectsOfTypeAll(typeof(VRButton)))
//            {
//                vrb.progressGraphics = new Image[] { vrb.progressGraphic };
//            }
//        }
//    }
//}