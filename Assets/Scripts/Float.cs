﻿using System.Collections;
using UnityEngine;

public class Float : MonoBehaviour
{
    [SerializeField] private float distanceFromStart = 0;
    [SerializeField] private float[] angleFromStart;
    [SerializeField] private float floatTime = 0;

    private Vector3 spawnPos;

    private void Start()
    {
        StartCoroutine(Floating());
        StartCoroutine(Rotating(0));
        StartCoroutine(Rotating(1));
    }

    private IEnumerator Floating()
    {
        int dir = 1;
        spawnPos = transform.localPosition;

        while (true)
        {
            Vector3 destination = spawnPos + transform.up * distanceFromStart * dir;
            Vector3 start = spawnPos + transform.up * distanceFromStart * -dir;

            float timer = 0;

            while (timer < floatTime)
            {
                transform.localPosition = Vector3.Lerp(start, destination, timer / floatTime);

                timer += Time.deltaTime;

                yield return 0;
            }

            dir *= -1;
        }
    }

    private IEnumerator Rotating(int axis)
    {
        if (angleFromStart.Length > 0)
        {
            int dir = 1;
            float spawnRot = transform.localEulerAngles[axis];

            float destination = spawnRot + Vector3.one[axis] * angleFromStart[axis] * dir;
            float start = spawnRot + Vector3.right[axis] * angleFromStart[axis] * -dir;

            while (true)
            {

                float timer = 0;

                while (timer < floatTime)
                {
                    Vector3 rot = transform.localEulerAngles;
                    rot[axis] = Mathf.Lerp(start, destination, timer / floatTime);
                    transform.localEulerAngles = rot;

                    timer += Time.deltaTime;

                    yield return 0;
                }

                dir *= -1;

                start = destination;
                destination = spawnRot + Vector3.one[axis] * angleFromStart[axis] * dir;
            }
        }
    }
}