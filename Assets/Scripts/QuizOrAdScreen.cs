﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class QuizOrAdScreen : LayoutScreen
{
    [SerializeField] private GameObject[] graphicsInSequence;

    public override IEnumerator Activate()
    {
        GameMode.CanButtonsBeUsed = false;

        for (int i = 0; i < graphicsInSequence.Length; i++)
        {
            MeshRenderer renderer;
            if (renderer = graphicsInSequence[i].GetComponent<MeshRenderer>())
            {
                Color color = renderer.material.color;
                color.a = 0;
                renderer.material.color = color;
            }
            else
            {
                foreach(Graphic g in graphicsInSequence[i].GetComponentsInChildren<Graphic>())
                {
                    Color color = g.color;
                    color.a = 0;
                    g.color = color;
                }
            }
        }

        gameObject.SetActive(true);

        for (int i = 0; i < graphicsInSequence.Length; i++)
        {
            MeshRenderer renderer;
            if (renderer = graphicsInSequence[i].GetComponent<MeshRenderer>())
            {
                float currTime = 0;
                fadeTime /= Mathf.Pow(1, 1.25f);
                while (currTime <= fadeTime)
                {
                    Color color = renderer.material.color;
                    color.a = Mathf.Lerp(0, 1, currTime / fadeTime);
                    renderer.material.color = color;

                    currTime += Time.deltaTime;

                    yield return 0;
                }
            }
            else
            {
                yield return GameMode.StartCoroutineOnMe(Fade(1, fadeTime, false, graphicsInSequence[i].GetComponentsInChildren<Graphic>()));
            }
        }

        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Start());

        GameMode.CanButtonsBeUsed = true;
    }
}