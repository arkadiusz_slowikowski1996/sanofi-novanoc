﻿using UnityEngine;

public class OpeningScreenButton : VRButton
{
    [SerializeField] private LayoutScreen screenToOpen;

    public override void Clicked()
    {
        base.Clicked();
        OpenScreen();
    }

    protected virtual void OpenScreen()
    {
        GameMode.ChangeScreen(screenToOpen);
    }
}