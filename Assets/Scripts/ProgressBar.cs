﻿using UnityEngine;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] private RectTransform fill;

    private RectTransform rectTransform;

    private float progress;
    public virtual float Progress
    {
        get => progress;
        set
        {
            value = Mathf.Clamp(value, 0, 1);

            Vector2 sizeDelta = fill.sizeDelta;
            sizeDelta.x = value * rectTransform.sizeDelta.x;
            fill.sizeDelta = sizeDelta;

            gameObject.SetActive(value > 0);

            progress = value;
        }
    }

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        Progress = 0;
    }
}