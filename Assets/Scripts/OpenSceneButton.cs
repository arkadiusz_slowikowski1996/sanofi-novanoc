﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenSceneButton : VRButton
{
    [SerializeField] private string sceneName;

    public override void Clicked()
    {
        base.Clicked();
        SceneManager.LoadScene(sceneName);
    }
}