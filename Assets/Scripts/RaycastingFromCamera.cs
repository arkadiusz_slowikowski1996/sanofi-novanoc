﻿using System.Collections;
using UnityEngine;

public class RaycastingFromCamera : MonoBehaviour
{
    [SerializeField] private float timeTillClick;
    [SerializeField] private ProgressBar progressBar;

    private VRButton previousVrButton;
    private float currentTimeTillClick = 2;

    private void FixedUpdate()
    {
        bool doElse = false;

        if (GameMode.CanButtonsBeUsed && Physics.Raycast(new Ray(transform.position, transform.forward), out RaycastHit hit, Mathf.Infinity))
        {
            VRButton vrButton = hit.collider.GetComponentInParent<VRButton>();

            if (vrButton && vrButton.canBeClicked && (!VRButton.generalForbid || vrButton.isBetterThanOther))
            {
                if (previousVrButton != vrButton)
                {
                    currentTimeTillClick = 0;
                    vrButton.Hovered();

                    if (previousVrButton)
                    {
                        previousVrButton.Unhovered();
                    }
                }

                if (!vrButton.RecentlyClicked)
                {
                    currentTimeTillClick += Time.fixedDeltaTime;
                    progressBar.Progress = currentTimeTillClick / timeTillClick;
                    vrButton.Progress = currentTimeTillClick / timeTillClick;
                }

                if (currentTimeTillClick >= timeTillClick)
                {
                    vrButton.Clicked();
                    currentTimeTillClick = 0;
                    vrButton.Progress = 0;
                    progressBar.Progress = 0;
                }
            }

            previousVrButton = vrButton;
            if (previousVrButton)
            {
                previousVrButton.Progress = currentTimeTillClick / timeTillClick;
            }
        }
        else
        {
            doElse = true;
        }

        if (doElse)
        {
            progressBar.Progress = 0;

            if (previousVrButton)
            {
                previousVrButton.Progress = progressBar.Progress;
                previousVrButton.Unhovered();
                previousVrButton = null;
            }
        }
    }
}