﻿using System.Collections;
using UnityEngine;

public class AdButton : OpeningScreenButton
{
    protected override void OpenScreen()
    {
        GameMode.StartCoroutineOnMe(CloseCurrentAndThenRun());
    }

    private IEnumerator CloseCurrentAndThenRun()
    {
        yield return GameMode.StartCoroutineOnMe(GetComponentInParent<LayoutScreen>().Deactivate());
        base.OpenScreen();

        GameMode.StartCoroutineOnMe(AdScreen.Run(true));
    }
}