﻿using UnityEngine;

public class LetMeLookAt : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private bool continous;

    private void Start()
    {
        RotateTowardsTarget();
    }

    private void Update()
    {
        if(continous)
        {
            RotateTowardsTarget();
        }
    }

    private void OnValidate()
    {
        RotateTowardsTarget();
    }

    private void RotateTowardsTarget()
    {
        transform.LookAt(2 * transform.position - target.position);
        Vector3 rot = transform.eulerAngles;
        rot.x = rot.z = 0;
        transform.eulerAngles = rot;
    }
}