﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Equipment : LayoutScreen
{
    private static Equipment instance;
    private static Equipment Instance
    {
        get
        {
            if (!instance)
            {
                Equipment[] allInstances = Resources.FindObjectsOfTypeAll(typeof(Equipment)) as Equipment[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    [SerializeField] private GameObject equipmentGameObject;
    [SerializeField] private Graphic ingrediesText;
    [SerializeField] private RectTransform[] inventorySlots;
    [SerializeField] private GameObject[] inactiveBackgrounds;
    [SerializeField] private GameObject[] inventorySlotsGlows;
    [SerializeField] private GameObject[] activeBackground;

    [SerializeField] private RectTransform[] gifs = new RectTransform[4];
    public static RectTransform[] Gifs {
        get  => Instance.gifs;
        set => Instance.gifs = value;
    }

    private static float movingGifTime = 1.5f;

    private void Awake()
    {
        Hypnogram.StageChanged += Hypnogram_StageChanged;

        for (int i = 0; i < Gifs.Length; i++)
        {
            Gifs[i] = inventorySlots[i];
        }
    }

    private void Hypnogram_StageChanged(int stage, int outOf)
    {
        Instance.equipmentGameObject.SetActive(true);

        GameMode.StartCoroutineOnMe(Fade(1, 3, false, Instance.equipmentGameObject.GetComponentsInChildren<Graphic>()));

        Hypnogram.StageChanged -= Hypnogram_StageChanged;
    }

    public static IEnumerator HideText()
    {
        float timer = 0;

        Color destColor = Color.white;
        destColor.a = 0;

        while(timer<Instance.fadeTime)
        {
            timer += Time.deltaTime;
            Instance.ingrediesText.color = Color.Lerp(Color.white, destColor, timer/Instance.fadeTime);

            yield return 0;
        }
    }

    public static IEnumerator Posses(RectTransform[] gifs, int[] slots)
    {
        IEnumerator FadeColor(Graphic graphic)
        {
            Color startColor = graphic.color;
            startColor.a = 0;

            Color finalColor = new Color(0, 46f / 225f, 106f / 225f, 1f);

            float time = 3;
            float timer = 0;

            while (timer < time)
            {
                graphic.color = Color.Lerp(startColor, finalColor, timer / time);
                timer += Time.deltaTime;
                yield return 0;
            }
        }

        for (int i = 0; i < slots.Length; i++)
        {
            RectTransform gifCopy = Instantiate(gifs[i]);
            gifCopy.gameObject.SetActive(true);
            Equipment.Gifs[slots[i]] = Instance.inventorySlots[slots[i]];//gifCopy;
            gifCopy.parent = Instance.inventorySlots[slots[i]];
            gifCopy.position = Instance.inventorySlots[slots[i]].position;
            gifCopy.sizeDelta = Instance.inventorySlots[slots[i]].sizeDelta;
            gifCopy.localScale = Vector3.one;

            Instance.activeBackground[slots[i]].SetActive(true);
            GameMode.StartCoroutineOnMe(FadeColor(gifCopy.GetComponent<Graphic>()));
            GameMode.StartCoroutineOnMe(Fade(0, 3, false, Instance.inactiveBackgrounds[slots[i]].GetComponent<Graphic>()));

            if (i != slots.Length - 1)
            {
                GameMode.StartCoroutineOnMe(Fade(1, 3, false, Instance.activeBackground[slots[i]].GetComponent<Graphic>()));
            }
            else
            {
                yield return GameMode.StartCoroutineOnMe(Fade(1, 3, false, Instance.activeBackground[slots[i]].GetComponent<Graphic>()));
            }
        }
    }

    public static IEnumerator PossesByMovement(RectTransform[] gifs, int[] slots)
    {
        Vector3[] startGifPoses = new Vector3[gifs.Length];
        Vector2[] startGifSizesDelta = new Vector2[gifs.Length];
        for (int i = 0; i < startGifPoses.Length; i++)
        {
            startGifPoses[i] = gifs[i].position;
            startGifSizesDelta[i] = gifs[i].sizeDelta;
        }

        float timer = 0;

        while (timer < movingGifTime)
        {
            timer += Time.deltaTime;

            for (int i = 0; i < gifs.Length; i++)
            {
                gifs[i].position = Vector3.Lerp(startGifPoses[i], Instance.inventorySlots[slots[i]].position, timer / movingGifTime);
                gifs[i].sizeDelta = Vector2.Lerp(startGifSizesDelta[i], Instance.inventorySlots[slots[i]].sizeDelta, timer / movingGifTime);
            }

            yield return 0;
        }

        for (int i = 0; i < gifs.Length; i++)
        {
            gifs[i].parent = Instance.inventorySlots[slots[i]];
            gifs[i].GetComponentInChildren<Image>().color = new Color(0, 46f / 225f, 106f / 225f, 1f);
            Color c = Instance.inventorySlots[slots[i]].GetComponentInParent<Image>().color;
            c.a = 1;
            Instance.inventorySlots[slots[i]].GetComponentInParent<Image>().color = c;
            Instance.inventorySlotsGlows[slots[i]].SetActive(true);
        }
    }
}