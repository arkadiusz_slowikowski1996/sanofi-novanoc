﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizableText : MonoBehaviour
{
    [Serializable]
    public class SerializableTextData
    {
        public string id;
        public string[] localizatedTextes;

        public int Id => int.Parse(id);
    }

    [Serializable]
    public class SerializableTextDatas
    {
        public SerializableTextData[] serializableTextDatas;
    }

    public enum TEXT_LANGUAGE { ENGLISH, POLISH }
    private static TEXT_LANGUAGE globalLanguage = TEXT_LANGUAGE.ENGLISH;

    [SerializeField] private int id = -1;
    [SerializeField][TextArea(1, 10)] private string[] localizatedText;

    static string path;

    private Text text;

    private TEXT_LANGUAGE MyLanguage
    {
        set
        {
            if (!text)text = GetComponent<Text>();
            text.text = localizatedText[(int)value];
        }
    }

    public static TEXT_LANGUAGE GlobalLanguage
    {
        set
        {
            if (globalLanguage != value)
            {
                globalLanguage = value;
                foreach (LocalizableText lt in Resources.FindObjectsOfTypeAll(typeof(LocalizableText)))
                    lt.MyLanguage = value;
            }
        }
    }

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    public void AssignNewID()
    {
        LocalizableText[] localizableTexts = Resources.FindObjectsOfTypeAll(typeof(LocalizableText))as LocalizableText[];

        int maxId = -1;

        foreach (LocalizableText lt in localizableTexts)
        {
            if (lt.id > maxId)
            {
                maxId = lt.id;
            }
            else if (lt.id == maxId && lt.id != -1)
            {
                Debug.LogError("Two objects got the same ID: " + maxId);
            }
        }

        Debug.Log("Max not taken ID: " + ++maxId + ". Assigned.");
        id = maxId;
    }

    public void ResetAllId()
    {
        LocalizableText[] localizableTexts = Resources.FindObjectsOfTypeAll(typeof(LocalizableText))as LocalizableText[];
        foreach (LocalizableText lt in localizableTexts)
        {
            lt.id = -1;
        }
    }

    public void GenerateAllId()
    {
        LocalizableText[] localizableTexts = Resources.FindObjectsOfTypeAll(typeof(LocalizableText))as LocalizableText[];
        foreach (LocalizableText lt in localizableTexts)
        {
            lt.AssignNewID();
        }
    }

    private void SwitchLanguage(TEXT_LANGUAGE newLanguage)
    {
        text.text = localizatedText[(int)globalLanguage];
    }

    public static void SerializeData()
    {
        string json = JsonUtility.ToJson(new SerializableTextDatas { serializableTextDatas = GetSerializableTextDatas().ToArray() });
        path = Application.dataPath + "\\Resources\\localizationTexts.txt";
        if (!File.Exists(path))File.Create(path);
        File.WriteAllText(path, json);
    }

    public static void DeserializeData()
    {
        SerializableTextDatas serializableTextDatas = JsonUtility.FromJson<SerializableTextDatas>(File.ReadAllText(path));

        //foreach (SerializableTextData std in serializableTextDatas.serializableTextDatas)
        //    SetSerializableTextDatas(std);
    }

    public static IEnumerable<SerializableTextData> GetSerializableTextDatas()
    {
        foreach (LocalizableText lt in Resources.FindObjectsOfTypeAll(typeof(LocalizableText)))
        {
            string[] array = new string[lt.localizatedText.Length];
            Array.Copy(lt.localizatedText, array, array.Length);

            yield return new SerializableTextData
            {
                id = lt.id.ToString(),
                    localizatedTextes = array
            };
        }
    }

    public static void SetSerializableTextDatas(SerializableTextData serializableTextData)
    {
        foreach (LocalizableText lt in Resources.FindObjectsOfTypeAll(typeof(LocalizableText)))
        {
            if (lt.id == serializableTextData.Id)
            {
                lt.text.text = serializableTextData.localizatedTextes[(int)globalLanguage];
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(LocalizableText))]
public class LocalizableText_Editor : Editor
{
    private new LocalizableText target;

    private void OnEnable()
    {
        target = (LocalizableText)base.target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.LabelField("0 - ENG, 1 - POL");
        if (GUILayout.Button("Generate ID"))
            target.AssignNewID();
        if (GUILayout.Button("Reset All ID"))
            target.ResetAllId();
        if (GUILayout.Button("Generate All ID"))
            target.GenerateAllId();
        if (GUILayout.Button("Serialize All Data"))
            LocalizableText.SerializeData();
        if (GUILayout.Button("Deserialize All Data"))
            LocalizableText.DeserializeData();
    }
}
#endif