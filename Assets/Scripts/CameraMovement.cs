﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float verticalSpeed = 1;
    [SerializeField] private float horizontalSpeed = 1;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
    }

    private void LateUpdate()
    {
        Vector3 currentEulers = transform.eulerAngles;
        currentEulers.y += horizontalSpeed * Input.GetAxis("Mouse X") * Time.deltaTime;
        currentEulers.x += verticalSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime;
        transform.eulerAngles= currentEulers;
    }
}