﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hypnogram : LayoutScreen
{
    public delegate void ChangingStage(int stage, int outOf);
    public static event ChangingStage StageChanged;

    public enum STAGE { FIRST, FALLING_ASLEEP, DEEP_SLEEP, FINAL }

    private static Hypnogram instance;
    private static Hypnogram Instance
    {
        get
        {
            if (!instance)
            {
                Hypnogram[] allInstances = Resources.FindObjectsOfTypeAll(typeof(Hypnogram))as Hypnogram[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    private static float disappearAfterTime = 5;
    private static float unfoldingTime = 2;

    [SerializeField] private RectTransform hypnogramTransform;
    [SerializeField] private Image[] phaseImages;
    [SerializeField] private Image additionalImage;
    [SerializeField] private GameObject[] additionalInfo;
    [SerializeField] private Transform bigTransform;
    [SerializeField] private Transform smallTransform;
    // [SerializeField] private Vector3 bigPos;
    // [SerializeField] private Vector3 bigScale;
    [SerializeField] private float bigAlpha;
    // [SerializeField] private Vector3 smallPos;
    // [SerializeField] private Vector3 smallScale;
    [SerializeField] private float smallAlpha;

    private static Image[] PhaseImages => Instance.phaseImages;

    private void Awake()
    {
        instance = this;
    }

    public static IEnumerator OpenCoroutine(STAGE stage)
    {
        foreach (Image i in Instance.phaseImages)
            i.gameObject.SetActive(false);

        Instance.additionalImage.gameObject.SetActive(false);

        ProgressGraph.StationsMode = ProgressGraph.STATIONS_MODE.HYPNOGRAM;
        StageChanged?.Invoke((int)stage, 3);
        //ProgressGraph.MoveToStage((int)stage);

        if (stage == STAGE.FIRST)
        {
            Instance.hypnogramTransform.gameObject.SetActive(true);
            yield return GameMode.StartCoroutineOnMe(Instance.Activate());
        }
        else
        {
            //Debug.Log("smallPosition " + Instance.bigTransform.localPosition);
            yield return GameMode.StartCoroutineOnMe(MoveTo(Instance.bigTransform.localPosition, Instance.bigTransform.localScale, Instance.bigAlpha));
        }

        //yield return GameMode.StartCoroutineOnMe(UnfoldImage(stage == STAGE.NONE ? null : PhaseImages[(int)stage ], stage == STAGE.DEEP_SLEEP ? 0 : 1));

        if (stage == STAGE.DEEP_SLEEP)
        {
            yield return GameMode.StartCoroutineOnMe(UnfoldImage(1, PhaseImages[(int)stage], Instance.additionalImage));
        }
        else if (stage == STAGE.FINAL)
        {
            List<Image> images = new List<Image>();
            images.AddRange(PhaseImages);
            images.Add(Instance.additionalImage);
            yield return GameMode.StartCoroutineOnMe(UnfoldImage(1, images.ToArray()));
        }
        else
        {
            yield return GameMode.StartCoroutineOnMe(UnfoldImage(1, PhaseImages[(int)stage]));
        }

        if (Instance.additionalInfo[(int)stage])
        {
            Instance.additionalInfo[(int)stage].SetActive(true);
            yield return GameMode.StartCoroutineOnMe(Fade(1, Instance.fadeTime / 2, false, Instance.additionalInfo[(int)stage].GetComponentsInChildren<Graphic>()));
        }

        //yield return new WaitForSeconds(disappearAfterTime / Time.timeScale);

        switch (stage)
        {
            case STAGE.FIRST:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Wykres1());
                break;
        }

        if (Instance.additionalInfo[(int)stage])
        {
            yield return GameMode.StartCoroutineOnMe(Fade(0, Instance.fadeTime / 2, true, Instance.additionalInfo[(int)stage].GetComponentsInChildren<Graphic>()));
        }
        //Debug.Log("smallPosition " + Instance.smallTransform.localPosition);

        yield return GameMode.StartCoroutineOnMe(MoveTo(Instance.smallTransform.localPosition, Instance.smallTransform.localScale, Instance.smallAlpha));
        //        yield return GameMode.StartCoroutineOnMe(Instance.Deactivate());

        ProgressGraph.StationsMode = ProgressGraph.STATIONS_MODE.QUESTION;
    }

    public static IEnumerator DeactivateWrap(bool sudden = false)
    {
        yield return GameMode.StartCoroutineOnMe(Instance.Deactivate(sudden));
    }

    private static IEnumerator MoveTo(Vector3 pos, Vector3 scale, float alpha)
    {
        float timer = 0;
        Graphic[] graphics = Instance.GetComponentsInChildren<Graphic>();
        Vector3 startPos = pos == Instance.smallTransform.localPosition ? Instance.bigTransform.localPosition : Instance.smallTransform.localPosition;
        Vector3 startScale = scale == Instance.smallTransform.localScale ? Instance.bigTransform.localScale : Instance.smallTransform.localScale;
        float startAlpha = alpha == Instance.smallAlpha ? Instance.bigAlpha : Instance.smallAlpha;

        while (timer < Instance.fadeTime)
        {
            timer += Time.deltaTime;

            Instance.hypnogramTransform.localPosition = Vector3.Lerp(startPos, pos, timer / Instance.fadeTime);
            Instance.hypnogramTransform.localScale = Vector3.Lerp(startScale, scale, timer / Instance.fadeTime);

            foreach (Graphic g in graphics)
            {
                Color color = g.color;
                color.a = Mathf.Lerp(startAlpha, alpha, timer / Instance.fadeTime);
                g.color = color;
            }

            yield return 0;
        }

        yield return new WaitForSeconds(1 / Time.timeScale);
    }

    private static IEnumerator UnfoldImage(int axis, params Image[] images)
    {
        float currTime = 0;
        float[] destValue = new float[images.Length];

        for (int i = 0; i < images.Length; i++)
        {
            destValue[i] = images[i].rectTransform.sizeDelta[axis];
            Vector2 currSize = images[i].rectTransform.sizeDelta;
            currSize[axis] = 0;
            images[i].rectTransform.sizeDelta = currSize;

            images[i].color = new Color(images[i].color.r, images[i].color.g, images[i].color.b, 1);
            images[i].gameObject.SetActive(true);
        }

        while (currTime <= unfoldingTime)
        {
            for (int i = 0; i < images.Length; i++)
            {
                Vector2 newSize = images[i].rectTransform.sizeDelta;
                newSize[axis] = destValue[i] * currTime / unfoldingTime;
                images[i].rectTransform.sizeDelta = newSize;
            }

            currTime += Time.deltaTime;

            yield return 0;
        }
    }
}