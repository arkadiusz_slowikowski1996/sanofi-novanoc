﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Locker : MonoBehaviour
{
    [SerializeField] private Image lockedImage;
    [SerializeField] private Image unlockedImage;

    public void TurnOffLockedImage()
    {
        GameMode.StartCoroutineOnMe(LayoutScreen.Fade(0, 5, true, lockedImage));
    }

    public void TurnOnUnlockedImage()
    {
        unlockedImage.gameObject.SetActive(true);
        GameMode.StartCoroutineOnMe(LayoutScreen.Fade(1, 5, false, unlockedImage));
    }
}