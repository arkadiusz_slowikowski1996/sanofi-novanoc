﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdScreen : LayoutScreen
{
    private static AdScreen instance;
    private static AdScreen Instance
    {
        get
        {
            if (!instance)
            {
                AdScreen[] allInstances = Resources.FindObjectsOfTypeAll(typeof(AdScreen)) as AdScreen[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    public static bool mayProceed = true;

    [SerializeField] private GameObject firstPart;
    [SerializeField] private GameObject secondPart;
    public static GameObject SecondPart => Instance.secondPart;

    [SerializeField] private MeshRenderer productPack;
    [SerializeField] private RectTransform togetherBackground;
    [SerializeField] private RectTransform[] ingrediensStages;
    [SerializeField] private RectTransform[] ingrediensHigher;
    [SerializeField] private RectTransform[] ingrediendOnStage;
    [SerializeField] private Transform gifsGroup;
    [SerializeField] private GameObject[] productFeatures;
    [SerializeField] private GameObject goBackScreen;

    private static float movingGifTime = 3;
    private static float movingIngrediensTime = 3;

    public static IEnumerator Run(bool skipFirstPart = false)
    {
        if (!skipFirstPart)
        {
            Instance.gameObject.SetActive(true);

            while (!mayProceed)
            {
                yield return 0;
            }

            yield return GameMode.StartCoroutineOnMe(Instance.PossesIngrediens());

            yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_KompletSkladnikow());

            yield return GameMode.StartCoroutineOnMe(Instance.PutIngrediensIntoMortar());

            //yield return GameMode.StartCoroutineOnMe(Instance.StirAndShowPill());

            GameMode.StartCoroutineOnMe(Hypnogram.DeactivateWrap());
            GameMode.StartCoroutineOnMe(ProgressGraph.DeactivateWrap());
        }

        Instance.secondPart.SetActive(true);

        foreach (GameObject go in Instance.productFeatures)
        {
            go.SetActive(false);
        }

        yield return GameMode.StartCoroutineOnMe(Mortar.Run());

        //yield return new WaitForSeconds(2);

        //yield return GameMode.StartCoroutineOnMe(Fade(0, 3, false, Instance.firstPart.GetComponentsInChildren<Graphic>()));
        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Novanoc());

        GameMode.StartCoroutineOnMe(Mortar.Hide());

        foreach (GameObject go in Instance.productFeatures)
        {
            go.SetActive(false);
        }

        yield return GameMode.StartCoroutineOnMe(Instance.FadeMesh());

        yield return GameMode.StartCoroutineOnMe(Fade(1, Instance.fadeTime / Time.timeScale, false, Instance.secondPart.GetComponentsInChildren<Graphic>()));

        GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Koniec());

        foreach (GameObject go in Instance.productFeatures)
        {
            go.SetActive(true);

            yield return GameMode.StartCoroutineOnMe(Fade(1, (Instance.fadeTime) / Time.timeScale, false, go.GetComponentsInChildren<Graphic>()));
        }

        while (LectorMode.isPlaying)
        {
            yield return 0;
        }

        foreach (GameObject go in Instance.productFeatures)
        {
            yield return GameMode.StartCoroutineOnMe(Fade(0, (Instance.fadeTime) / Time.timeScale, false, go.GetComponentsInChildren<Graphic>()));
        }

        VRButton.generalForbid = true;
        foreach (GameObject go in Instance.productFeatures)
        {
            go.SetActive(false);
        }

        Instance.secondPart.SetActive(false);

        // VRButton[] vrButtons = Instance.goBackScreen.GetComponentsInChildren<VRButton>();
        // foreach(VRButton vb in vrButtons)
        // {
        //     vb.enabled = false;
        // }

        Instance.goBackScreen.SetActive(true);

        yield return GameMode.StartCoroutineOnMe(Fade(1, (Instance.fadeTime) / Time.timeScale, false, Instance.goBackScreen.GetComponentsInChildren<Graphic>()));

        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Dziekujemy());
        VRButton.generalForbid = false;
        // foreach(VRButton vb in vrButtons)
        // {
        //     vb.enabled = true;
        // }
    }

    private IEnumerator PossesIngrediens()
    {
        GameMode.StartCoroutineOnMe(Equipment.HideText());

        Vector3[] startGifPoses = new Vector3[Equipment.Gifs.Length];
        Vector2[] startGifSizesDelta = new Vector2[Equipment.Gifs.Length];

        for (int i = 0; i < startGifPoses.Length; i++)
        {
            Equipment.Gifs[i].parent = gifsGroup.GetChild(i);
            startGifPoses[i] = Equipment.Gifs[i].position;
            startGifSizesDelta[i] = Equipment.Gifs[i].sizeDelta;
        }

        float timer = 0;

        while (timer < movingGifTime)
        {
            timer += Time.deltaTime;

            for (int i = 0; i < Equipment.Gifs.Length; i++)
            {
                Equipment.Gifs[i].position = Vector3.Lerp(startGifPoses[i], Instance.ingrediensHigher[i].position, timer / movingGifTime);
                Equipment.Gifs[i].sizeDelta = Vector2.Lerp(startGifSizesDelta[i], Instance.ingrediensHigher[i].sizeDelta, timer / movingGifTime);
            }

            yield return 0;
        }

        for (int i = 0; i < Equipment.Gifs.Length; i++)
        {
            ingrediendOnStage[i] = Equipment.Gifs[i];
        }
    }

    private IEnumerator FadeMesh()
    {
        productPack.gameObject.SetActive(true);
        float currTime = 0;
        fadeTime /= Mathf.Pow(1, 1.25f);
        while (currTime <= fadeTime)
        {
            Color color = productPack.material.color;
            color.a = Mathf.Lerp(0, 1, currTime / fadeTime);
            productPack.material.color = color;

            currTime += Time.deltaTime;

            yield return 0;
        }
    }

    //private IEnumerator PossesIngrediens()
    //{
    //    Vector3[] startGifPoses = new Vector3[Equipment.gifs.Length];
    //    Vector2[] startGifSizesDelta = new Vector2[Equipment.gifs.Length];

    //    for (int i = 0; i < startGifPoses.Length; i++)
    //    {
    //        Equipment.gifs[i].parent = mortarGif.transform;
    //        startGifPoses[i] = Equipment.gifs[i].position;
    //        startGifSizesDelta[i] = Equipment.gifs[i].sizeDelta;
    //    }

    //    float timer = 0;

    //    while (timer < movingGifTime)
    //    {
    //        timer += Time.deltaTime;

    //        for (int i = 0; i < Equipment.gifs.Length; i++)
    //        {
    //            Equipment.gifs[i].position = Vector3.Lerp(startGifPoses[i], Instance.ingrediensStages[i].position, timer / movingGifTime);
    //            Equipment.gifs[i].sizeDelta = Vector2.Lerp(startGifSizesDelta[i], Instance.ingrediensStages[i].sizeDelta, timer / movingGifTime);
    //        }

    //        yield return 0;
    //    }

    //    for (int i = 0; i < Equipment.gifs.Length; i++)
    //    {
    //        ingrediendOnStage[i] = Equipment.gifs[i];
    //    }
    //}

    private IEnumerator PutIngrediensIntoMortar()
    {
        Vector3 finalPos = Instance.ingrediensHigher[0].position;
        finalPos.x = 0;

        Vector3[] startPoses = new Vector3[Instance.ingrediensHigher.Length];

        for (int i = 0; i < Equipment.Gifs.Length; i++)
        {
            startPoses[i] = Equipment.Gifs[i].position;
        }

        float timer = 0;
        while (timer < movingIngrediensTime)
        {
            for (int i = 0; i < Equipment.Gifs.Length; i++)
            {
                Equipment.Gifs[i].position = Vector3.Lerp(startPoses[i], finalPos, timer / movingIngrediensTime);
            }

            timer += Time.deltaTime;

            yield return 0;
        }

        foreach (RectTransform rt in Equipment.Gifs)
        {
            rt.GetChild(0).gameObject.SetActive(false);
            rt.GetChild(1).gameObject.SetActive(false);
        }

        togetherBackground.position = Equipment.Gifs[0].position;
        togetherBackground.gameObject.SetActive(true);

        //Vector3[] startDeltas = new Vector3[startPoses.Length];
        for (int i = 0; i < Equipment.Gifs.Length; i++)
        {
            //startDeltas[i] = Equipment.Gifs[i].sizeDelta;
            //Vector2 recc = new Vector2((togetherBackground.GetChild(i) as RectTransform).rect.width,
            //    (togetherBackground.GetChild(i) as RectTransform).rect.height);

            //Equipment.Gifs[i].localScale = Instance.gifsGroup.GetChild(i).localScale;
            Equipment.Gifs[i].sizeDelta = Instance.gifsGroup.GetChild(i).GetComponent<RectTransform>().sizeDelta;
            Equipment.Gifs[i].localScale = Vector3.one;
            Equipment.Gifs[i].parent.localScale = Vector3.one;
            startPoses[i] = togetherBackground.position;
        }

        timer = 0;
        float totalTime = movingIngrediensTime / 3;
        while (timer < totalTime)
        {
            for (int i = 0; i < Equipment.Gifs.Length; i++)
            {
                //Equipment.Gifs[i].parent.localPosition = Vector3.Lerp(startPoses[i], Vector3.zero, timer / totalTime);
                Equipment.Gifs[i].position = Vector3.Lerp(startPoses[i], Equipment.Gifs[i].parent.position, timer / totalTime);
                //Equipment.Gifs[i].sizeDelta = Vector3.Lerp(startDeltas[i], (togetherBackground.GetChild(i) as RectTransform).sizeDelta, timer / movingIngrediensTime);
            }

            timer += Time.deltaTime;

            yield return 0;
        }

        yield return new WaitForSeconds(3 / Time.timeScale);

        List<Graphic> graphics = new List<Graphic>();

        for (int i = 0; i < gifsGroup.transform.childCount; i++)
        {
            graphics.AddRange(gifsGroup.transform.GetChild(i).GetComponentsInChildren<Graphic>());
        }

        GameMode.StartCoroutineOnMe(Fade(0, 3, false, graphics.ToArray()));
        GameMode.StartCoroutineOnMe(Fade(0, 3, false, togetherBackground.GetComponent<Graphic>()));
    }

    //private IEnumerator PutIngrediensIntoMortar()
    //{
    //    int progress = 0;
    //    for (int j = 0; j < Instance.ingrediensStages.Length + 1; j++)
    //    {
    //        for (int i = 0; i < ingrediensStages.Length; i++)
    //        {
    //            if (i + progress < ingrediendOnStage.Length)
    //            {
    //                Equipment.gifs[i].position = Instance.ingrediensStages[i + progress].position;
    //                Equipment.gifs[i].sizeDelta = Instance.ingrediensStages[i + progress].sizeDelta;
    //            }
    //            else
    //            {
    //                Equipment.gifs[i].gameObject.SetActive(false);
    //            }
    //        }

    //        yield return new WaitForSeconds(movingIngrediensTime);

    //        progress++;
    //    }
    //}

    //private IEnumerator StirAndShowPill()
    //{
    //    List<Graphic> graphics = new List<Graphic>();

    //    for (int i = 0; i < gifsGroup.transform.childCount; i++)
    //    {
    //        graphics.AddRange(gifsGroup.transform.GetChild(i).GetComponentsInChildren<Graphic>());
    //    }

    //    GameMode.StartCoroutineOnMe(Fade(0, 3, false, graphics.ToArray()));
    //    GameMode.StartCoroutineOnMe(Fade(0, 3, false, togetherBackground.GetComponent<Graphic>()));
    //    yield return GameMode.StartCoroutineOnMe(Fade(1, 3, false, gifsGroup.GetComponent<Graphic>()));

    //    gifsGroup.enabled = true;
    //    yield return new WaitForSeconds(gifsGroup.TimeDuration);
    //    gifsGroup.enabled = false;
    //}
}