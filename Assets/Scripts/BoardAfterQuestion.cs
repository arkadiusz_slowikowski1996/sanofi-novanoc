﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardAfterQuestion : LayoutScreen
{
    public delegate void IngredientActing();
    public static event IngredientActing NewIngredientAppeared;
    public static event IngredientActing NewIngredientPossesed;

    [SerializeField] private GameObject[] graphicsInSequence;
    [SerializeField] private RectTransform[] myGifs;
    [SerializeField] private int[] slotsInInventoryForMyGifs;

    public IEnumerator Run()
    {
        foreach(Graphic g in GetComponentsInChildren<Graphic>())
        {
            Color c = g.color;
            c.a = 0;
            g.color = c;
        }

        GameMode.StartCoroutineOnMe(ShowGraphics());

        yield return GameMode.StartCoroutineOnMe(PlayLector());

        NewIngredientAppeared?.Invoke();

        yield return new WaitForSeconds(1/Time.timeScale);

        // List<Graphic> graphics = new List<Graphic>();

        // foreach(var r in myGifs)
        // {
        //     graphics.AddRange(r.GetComponentsInChildren<Graphic>());
        // }

        // yield return GameMode.StartCoroutineOnMe(Fade(0, 3, false, graphics.ToArray()));

        // foreach(var r in myGifs)
        // {
        //     r.gameObject.SetActive(false);
        // }

        yield return GameMode.StartCoroutineOnMe(Equipment.PossesByMovement(myGifs, slotsInInventoryForMyGifs));

        NewIngredientPossesed?.Invoke();

        yield return new WaitForSeconds(1/Time.timeScale);

        yield return GameMode.StartCoroutineOnMe(Fade(0, fadeTime, false, GetComponentsInChildren<Graphic>()));

        yield return 0;
    }

    private IEnumerator ShowGraphics()
    {
        for (int i = 0; i < graphicsInSequence.Length; i++)
        {
            yield return GameMode.StartCoroutineOnMe(Fade(1, fadeTime, false, graphicsInSequence[i].GetComponentsInChildren<Graphic>()));
        }
    }

    private IEnumerator PlayLector()
    {
        switch (slotsInInventoryForMyGifs[0])
        {
            case 0:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Skladnik1());
                break;
            case 1:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Skladnik2());
                break;
            case 3:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Skladnik3());
                break;
        }
    }
}