﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Mortar : MonoBehaviour
{
    private static Mortar instance;
    private static Mortar Instance
    {
        get
        {
            if (!instance)
            {
                Mortar[] allInstances = Resources.FindObjectsOfTypeAll(typeof(Mortar)) as Mortar[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    [SerializeField] private Transform objectToObserve;
    [SerializeField] private RectTransform fill;
    [SerializeField] private RectTransform hammer;
    [SerializeField] private Vector3 minRot;
    [SerializeField] private Vector3 maxRot;
    [SerializeField] private Vector3 minPos;
    [SerializeField] private Vector3 maxPos;
    [SerializeField] private float maxProgress;
    [SerializeField] private int progressRuns;
    [SerializeField] private float progress;
    [Header("---bubbles---")]
    [SerializeField] private RectTransform bubblesSpawnArea;
    [SerializeField] private MortarBubble[] bubblesPrefabs;
    [SerializeField] private float bubblesMilestonesEveryWhat;
    [SerializeField] private float bubblesSpeed;
    [SerializeField] private float bubblesLyfeTyme;
    [SerializeField] private float bubblesDyingTime;

    [SerializeField] private float oneRunProgress;

    public static IEnumerator Hide(Func<int> after = null)
    {
        yield return GameMode.StartCoroutineOnMe(LayoutScreen.Fade(0, 3, false, Instance.GetComponentsInChildren<Graphic>()));
        Instance.gameObject.SetActive(false);
        after?.Invoke();
    }

    public static IEnumerator Run()
    {
        Instance.gameObject.SetActive(true);
        GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Mozdzierz());
        yield return GameMode.StartCoroutineOnMe(LayoutScreen.Fade(1, 3, false, Instance.GetComponentsInChildren<Graphic>()));

        GameMode.StartCoroutineOnMe(Instance.CountOneRunProgress());
        GameMode.StartCoroutineOnMe(Instance.MoveHammer());
        GameMode.StartCoroutineOnMe(Instance.MakeBubbles());

        Quaternion prevrot = Instance.objectToObserve.rotation;

        while (Instance.progress < Instance.maxProgress)
        {
            Instance.progress += Quaternion.Angle(prevrot, Instance.objectToObserve.rotation);
            prevrot = Instance.objectToObserve.rotation;

            Vector2 delta = Instance.fill.sizeDelta;
            delta.y = (Instance.fill.transform.parent as RectTransform).sizeDelta.y;
            delta.x = (Instance.fill.transform.parent as RectTransform).sizeDelta.x * Instance.progress / Instance.maxProgress;
            Instance.fill.sizeDelta = delta;

            yield return 0;
        }
    }

    private IEnumerator CountOneRunProgress()
    {
        while (true)
        {
            oneRunProgress = progress / maxProgress;
            oneRunProgress = Mathf.PingPong(oneRunProgress, 1f / progressRuns) / (1f / progressRuns);
            yield return 0;
        }
    }

    private IEnumerator MoveHammer()
    {
        while (true)
        {
            hammer.localPosition = Vector3.Lerp(minPos, maxPos, oneRunProgress);
            hammer.localEulerAngles = Vector3.Lerp(minRot, maxRot, oneRunProgress);

            yield return 0;
        }
    }

    private IEnumerator MakeBubbles()
    {
        int i = 0;

        while (true)
        {
            if (progress / maxProgress > bubblesMilestonesEveryWhat * i)
            {
                i++;

                Vector2 randomizedPosition = Vector2.zero;
                randomizedPosition.x = Random.Range(bubblesSpawnArea.rect.xMin, bubblesSpawnArea.rect.xMax);
                randomizedPosition.y = Random.Range(bubblesSpawnArea.rect.yMin, bubblesSpawnArea.rect.yMax);

                int newBubbleIndex = Random.Range(0, bubblesPrefabs.Length);

                Instantiate(
                    bubblesPrefabs[newBubbleIndex],
                    Vector3.zero,
                    Quaternion.identity,
                    bubblesSpawnArea)
                .Setup(bubblesSpeed, bubblesLyfeTyme, bubblesDyingTime, randomizedPosition);
            }

            yield return 0;
        }
    }
}