﻿using System.Collections;
using UnityEngine;

public class AudioMode : MonoBehaviour
{
    [SerializeField] private AudioClip backgroundClip;
    [SerializeField] private float backgroundClipVolume;
    [Header("---")]
    [SerializeField] private AudioClip logoshowingClip;
    [SerializeField] private float logoshowingClipVolume;
    [Header("---")]
    [SerializeField] private AudioClip newQuestionClip;
    [SerializeField] private float newQuestionClipVolume;
    [Header("---")]
    [SerializeField] private AudioClip goodAnswerClip;
    [SerializeField] private float goodAnswerClipVolume;
    [Header("---")]
    [SerializeField] private AudioClip badAnswerClip;
    [SerializeField] private float badAnswerClipVolume;

    [Header("\t")]
    [SerializeField] private AudioSource audioSource;

    private void Awake()
    {
        GameMode.LogoShown += () => audioSource.PlayOneShot(logoshowingClip, logoshowingClipVolume);
    }

    private IEnumerator Start()
    {
        yield return 0;

        audioSource.volume = backgroundClipVolume;
        audioSource.clip = backgroundClip;
        audioSource.Play();
    }

    private void Update()
    {
        audioSource.pitch = Time.timeScale;
    }
}