﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : VRButton
{
    public delegate void GivingAnswer(bool isCorrect);
    public static event GivingAnswer AnswerGiven;

    [SerializeField] private Question myQuestion;
    [SerializeField] private Image image;

    private static float changingColorTime = 1;

    private new void OnValidate()
    {
        base.OnValidate();
        if (!image) image = GetComponentInChildren<Image>();
        if (!myQuestion) myQuestion = GetComponentInParent<Question>();
    }

    public override void Clicked()
    {
        base.Clicked();

        GameMode.StartCoroutineOnMe(Clicked_Coroutine());
    }

    private IEnumerator Clicked_Coroutine()
    {
        bool amICorrect = myQuestion.GiveAnswer(this);

        canBeClicked = false;

        StartCoroutine(ChangeColor(amICorrect ? Color.green : Color.red));

        AnswerGiven?.Invoke(amICorrect);

        yield return GameMode.StartCoroutineOnMe(PlayLector());

        if (amICorrect) myQuestion.Continue();
    }

    private IEnumerator ChangeColor(Color destColor)
    {
        float timer = 0;

        while (timer < changingColorTime)
        {
            timer += Time.deltaTime;

            foreach (Graphic g in AllMyGraphics)
            {
                g.color = Color.Lerp(Color.white, destColor, timer / changingColorTime);
            }

            yield return 0;
        }
    }

    private IEnumerator PlayLector()
    {
        //GameMode.CanButtonsBeUsed = false;

        switch (((int)myQuestion.hypnogramStageAfterMe))
        {
            case 1:
                switch (transform.GetSiblingIndex())
                {
                    case 0:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp1_1());
                        break;
                    case 1:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp1_2());
                        break;
                    case 2:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp1_3());
                        break;
                }
                break;
            case 2:
                switch (transform.GetSiblingIndex())
                {
                    case 0:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp2_1());
                        break;
                    case 1:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp2_2());
                        break;
                    case 2:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp2_3());
                        break;
                }
                break;
            case 3:
                switch (transform.GetSiblingIndex())
                {
                    case 0:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp3_1());
                        break;
                    case 1:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp3_2());
                        break;
                    case 2:
                        yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Odp3_3());
                        break;
                }
                break;
        }

        //GameMode.CanButtonsBeUsed = true;
    }
}