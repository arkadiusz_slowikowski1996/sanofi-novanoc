﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class CommercialHandler : MonoBehaviour
{
    private static CommercialHandler instance;
    private static CommercialHandler Instance
    {
        get
        {
            if (!instance)
            {
                CommercialHandler[] allInstances = Resources.FindObjectsOfTypeAll(typeof(CommercialHandler)) as CommercialHandler[];
                if (allInstances.Length == 1)
                    instance = allInstances[0];
                else
                    Debug.LogError("Fucked up length of found instances array: " + allInstances.Length);
            }

            return instance;
        }
    }

    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private Image screen;

    private void Awake()
    {
        instance = this;
    }

    public static IEnumerator Run()
    {
        Instance.gameObject.SetActive(true);
        Instance.videoPlayer.Prepare();

        while(!Instance.videoPlayer.isPrepared)
        {
            yield return 0;
        }

        Instance.screen.color = Color.white;
        Instance.screen.canvasRenderer.SetTexture(Instance.videoPlayer.texture);
        Instance.videoPlayer.Play();
        
        while(Instance.videoPlayer.isPlaying)
        {
            yield return 0;
        }
    }
}