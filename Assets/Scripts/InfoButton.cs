﻿using UnityEngine;

public class InfoButton : VRButton
{
    [SerializeField] private bool buttonOpeningInfoBoard;
    [SerializeField] private GameObject screenToOpen;

    public override void Clicked()
    {
        base.Clicked();
        screenToOpen.SetActive(buttonOpeningInfoBoard);
        VRButton.generalForbid = buttonOpeningInfoBoard;
    }
}