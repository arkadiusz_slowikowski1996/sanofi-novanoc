﻿using System.Collections;
using UnityEngine;

public class QuizScreen : LayoutScreen
{
    private static QuizScreen instance;

    [SerializeField] private Question[] questions = new Question[0];

    private static int currentQuestion = -1;

    private void Awake()
    {
        if (instance)
        {
            Destroy(instance.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        if (questions.Length == 0) questions = GetComponentsInChildren<Question>();
        GameMode.StartCoroutineOnMe(ChangeToNextQuestionCoroutine());
    }

    public static void ChangeToNextQuestion()
    {
        GameMode.StartCoroutineOnMe(ChangeToNextQuestionCoroutine());
    }

    private static IEnumerator ChangeToNextQuestionCoroutine()
    {
        if (currentQuestion > -1)
        {
            yield return GameMode.StartCoroutineOnMe(instance.questions[currentQuestion].Deactivate());
            yield return GameMode.StartCoroutineOnMe(Hypnogram.OpenCoroutine(instance.questions[currentQuestion].hypnogramStageAfterMe));
        }

        ++currentQuestion;
        if (currentQuestion < instance.questions.Length)
        {
            yield return GameMode.StartCoroutineOnMe(instance.questions[currentQuestion].Activate());
        }
        else GameMode.StartCoroutineOnMe(AdScreen.Run());
    }
}