﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Question : LayoutScreen
{
    public delegate void QuestionAppearing();
    public static event QuestionAppearing NewQuestionAppeared;

    private static float movingSpeed = 1;
    private static float timeToWaitAfterCorrectAnswer = 3;

    public Hypnogram.STAGE hypnogramStageAfterMe;

    [SerializeField] private RectTransform questionBorder;
    [SerializeField] private AnswerButton[] myButtons;
    [SerializeField] private Transform[] buttonsStartPositions;
    [SerializeField] private Transform[] buttonsFinalPositions;
    [SerializeField] private AnswerButton correctAnswer;
    [SerializeField] private BoardAfterQuestion boardAfterQuestion;

    private bool answered;

    public override IEnumerator Activate()
    {
        GameMode.CanButtonsBeUsed = false;
        Graphic[] graphics = null;

        Vector3 questionBorderOriginalScale = questionBorder.localScale;
        questionBorder.localScale = 1.5f * questionBorder.localScale;

        for (int i = 0; i < myButtons.Length; i++)
        {
            myButtons[i].transform.position = buttonsStartPositions[i].position;
            myButtons[i].gameObject.SetActive(false);

            graphics = myButtons[i].AllMyGraphics;

            foreach (Graphic g in graphics)
            {
                g.color = new Color(g.color.r,
                     g.color.g,
                      g.color.b,
                      0);
            }
        }

        NewQuestionAppeared?.Invoke();

        yield return GameMode.StartCoroutineOnMe(base.Activate());

        switch ((int)hypnogramStageAfterMe)
        {
            case 1:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Pytanie1());
                break;
            case 2:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Pytanie2());
                break;
            case 3:
                yield return GameMode.StartCoroutineOnMe(LectorMode.PlayWithWaiting_Pytanie3());
                break;

            default:
                Debug.LogError("not expected " + (int)hypnogramStageAfterMe);
                break;
        }

        float time = 1;
        float timer = 0;
        while (timer <= time)
        {
            questionBorder.localScale = Vector2.Lerp(1.5f * questionBorderOriginalScale, questionBorderOriginalScale, timer / time);
            timer += Time.deltaTime;
            yield return 0;
        }

        yield return GameMode.StartCoroutineOnMe(MoveButtons());
        GameMode.CanButtonsBeUsed = true;
    }

    private IEnumerator MoveButtons()
    {
        for (int i = 0; i < myButtons.Length; i++)
        {
            myButtons[i].gameObject.SetActive(true);
            GameMode.StartCoroutineOnMe(MoveButton(myButtons[i], buttonsFinalPositions[i].position));

            yield return new WaitForSeconds(1.5f/Time.timeScale);
        }
    }

    private IEnumerator MoveButton(AnswerButton myButton, Vector3 finalPosition)
    {
        while (myButton.transform.position != buttonsFinalPositions[0].position)
        {
            Graphic[] graphics = null;

            myButton.transform.position = Vector3.Lerp(myButton.transform.position, finalPosition, movingSpeed * Time.deltaTime);

            graphics = myButton.AllMyGraphics;

            foreach (Graphic g in graphics)
            {
                Color c = g.color;
                c.a = 1;
                g.color = Color.Lerp(g.color, c, movingSpeed * Time.deltaTime);
            }

            yield return 0;
        }
    }

    //private IEnumerator MoveButtons()
    //{
    //    float currTime = 0;
    //    while (myButtons[0].transform.position != buttonsFinalPositions[0].position)
    //    {
    //        Graphic[] graphics = null;

    //        for (int i = 0; i < myButtons.Length; i++)
    //        {
    //            myButtons[i].transform.position = Vector3.Lerp(myButtons[i].transform.position, buttonsFinalPositions[i].position, movingSpeed * Time.deltaTime);

    //            graphics = myButtons[i].AllMyGraphics;

    //            foreach (Graphic g in graphics)
    //            {
    //                Color c = g.color;
    //                c.a = 1;
    //                g.color = Color.Lerp(g.color, c, movingSpeed * Time.deltaTime);
    //            }
    //        }

    //        currTime += Time.deltaTime;

    //        yield return 0;
    //    }
    //}

    public bool GiveAnswer(AnswerButton answer)
    {
        answered = correctAnswer == answer;

        if(answered)
        {
            foreach(AnswerButton ab in myButtons)
            {
                ab.canBeClicked = false;
            }
        }

        return answered;
    }

    public void Continue()
    {
        if (answered)
            GameMode.StartCoroutineOnMe(CorrectAnswerCoroutine());
    }

    private IEnumerator CorrectAnswerCoroutine()
    {
        foreach (AnswerButton ab in myButtons)
            ab.canBeClicked = false;

        //yield return new WaitForSeconds(timeToWaitAfterCorrectAnswer/Time.timeScale);
        yield return GameMode.StartCoroutineOnMe(Fade(0, fadeTime, false, GetComponentsInChildren<Graphic>()));
        for (int i = 0; i < 2; i++)
            transform.GetChild(i).gameObject.SetActive(false);
        yield return GameMode.StartCoroutineOnMe(RunBoardAfterQuestion());
    }

    private IEnumerator RunBoardAfterQuestion()
    {
        yield return new WaitForSeconds(1/Time.timeScale);
        boardAfterQuestion.gameObject.SetActive(true);
        yield return GameMode.StartCoroutineOnMe(boardAfterQuestion.Run());
        boardAfterQuestion.gameObject.SetActive(false);
        QuizScreen.ChangeToNextQuestion();
    }
}