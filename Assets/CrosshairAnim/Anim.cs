﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Anim : MonoBehaviour
{
    [SerializeField] private Transform stary;
    [SerializeField] private float time = 5;
    [SerializeField] private Image circle;

    //28.8 || 0.08

    private IEnumerator Start()
    {
        yield return StartCoroutine(Fade(0, 1f, false, GetComponentsInChildren<Graphic>()));

        yield return new WaitForSeconds(1);
        StartCoroutine(Fade(1, 1, false, GetComponentsInChildren<Graphic>()));

        yield return StartCoroutine(Play());
    }

    private IEnumerator Play()
    {
        float timer = 0;

        circle.fillAmount = 0.08f;
        stary.localEulerAngles = Vector3.forward * 50;
        transform.eulerAngles = Vector3.forward * 250;

        while (true)
        {
            //stary.localEulerAngles = Vector3.forward * (290 * Mathf.Clamp(timer / (time * 0.9f), 0, 1) + 50.8f);
            //stary.GetChild(0).localEulerAngles = -stary.localEulerAngles;
            stary.GetChild(0).eulerAngles = Vector3.zero;

            stary.localEulerAngles = Vector3.Lerp(stary.localEulerAngles, Vector3.forward * 330, time * Time.deltaTime);// Vector3.forward * (290 * Mathf.Clamp(timer / (time * 0.9f), 0, 1) + 50.8f);
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Vector3.forward * 315, time * Time.deltaTime);// Vector3.forward * (290 * Mathf.Clamp(timer / (time * 0.9f), 0, 1) + 50.8f);

            //circle.fillAmount = 0.92f * timer / time + 0.08f;
            circle.fillAmount = Mathf.Lerp(circle.fillAmount, 0.92f, time * Time.deltaTime);

            //transform.eulerAngles += Vector3.forward * (1 - timer / time)*120* Time.deltaTime;

            timer += Time.deltaTime;
            if (circle.fillAmount >= 0.9)
                Debug.Log("YEAH");
            yield return 0;
        }
    }

    public static IEnumerator Fade(int destAlpha, float fadeTime, bool turnOffWhenDone, params Graphic[] targets)
    {
        float currTime = 0;
        fadeTime /= Mathf.Pow((1 + (1 - destAlpha)), 1.25f);
        while (currTime <= fadeTime)
        {
            foreach (Graphic g in targets)
            {
                Color color = g.color;
                color.a = Mathf.Lerp(1 - destAlpha, destAlpha, currTime / fadeTime);
                g.color = color;
            }

            currTime += Time.deltaTime;

            yield return 0;
        }

        if (turnOffWhenDone)
            foreach (Graphic g in targets)
                g.gameObject.SetActive(false);
    }
}